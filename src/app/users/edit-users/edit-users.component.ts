import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { FormBuilder } from '@angular/forms';
import { User } from '../model/user';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.css']
})
export class EditUsersComponent implements OnInit {

  constructor(
    private activatedRoute : ActivatedRoute,
    private service : UserService,
    private formBuilder : FormBuilder,
    private router : Router){}

  user? : User;
  editForm = this.formBuilder.group({
    name : '',
    email : '',
    password : ''
  });
 



  editUser = () => {
    const values = this.editForm.value;
    this.service.editUser(
      new User(this.user!.id, values.name!, values.email!, values.password!)
    ).subscribe(
      user => {
        this.router.navigate(['/users']);
        Swal.fire('Success', 'User edited successfully!', 'success');
      },
      error => {
        console.log(error);
        Swal.fire('Error', 'An error occurred while editing the user.', 'error');
      }
    );
  }
  


  ngOnInit(): void {
    this.activatedRoute.params.subscribe(
      params=>{
        this.service.getUserById(+params['id']).subscribe(
          user=> {
            this.user = user;

            this.editForm.setValue({
              name : this.user.name,
              email : this.user.email,
              password : this.user.password + ''
            })
          }
        )
      }
    )

  }

}

