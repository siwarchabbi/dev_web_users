import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { Subscription } from 'rxjs';
import { UserService } from '../service/user.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  users? : User[];
  subscription? : Subscription;

  constructor(private service : UserService){}

  deleteUser = (id: number) => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this user!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.deleteUser(id).subscribe(
          obj => {
            this.users = this.users?.filter(user => user.id !== id);
            Swal.fire('Deleted!', 'The user has been deleted.', 'success');
          },
          error => {
            console.log(error);
            Swal.fire('Error', 'An error occurred while deleting the user.', 'error');
          },
          () => console.log('Terminé')
        );
      }
    });
  }
  
  ngOnInit(): void {
    this.service.getUsers().subscribe(
      users => this.users = users
    );

  }

}
